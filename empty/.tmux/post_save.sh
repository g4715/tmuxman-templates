#!/usr/bin/env bash
set -o nounset
set -o pipefail

if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

main () {
    workspace=$1
    hostname=$HOSTNAME
    user=$USER
    resurrectdir=$HOME/my_data/${workspace}/.tmux/resurrect
    file=$(readlink ${resurrectdir}/last)

    sed -e "s/${hostname}/<hostname>/g" \
        -e "s/${user}/<user>/g" \
        -e "s/${workspace}/<workspace>/g" \
        ${resurrectdir}/${file} > \
        ${resurrectdir}/saved.txt
}

main "$@"
