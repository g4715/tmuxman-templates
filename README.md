# tmuxman templates

## What it is:
Here you will find some handy directory trees along with preformatted .md files
and tmux-resurrect snapshots.

## Why it exists:
It is nice to have an ordertly directory of reusable workspaces that are easy
to copy and update as need arises. 

## What it does and how to use
See the tmuxman repo for usage examples and deeper logic behind the process.
