<BOX-NAME>
{{{
# System
{{{
OS:
Arch:
Kernel:
Hostname:
}}}
# Credentials
{{{
## <SERVICE>
<USER>:<PASSWORD>
}}}
# Submissions
{{{
user.txt
<user-hash>
root.txt
<root-hash>
}}}
# Initial Enumeration
{{{
## nmap
{{{
<NMAP-OUTPUT>
}}}
## Targets
{{{
<PORT> -- <SERVICE1>
<PORT> -- <SERVICE2>
<PORT> -- <SERVICE3>
}}}
}}}
# Active Enumeration
{{{
## <SERVICE1>
{{{
### Tools
{{{
#### <TOOL1>
#### <TOOL2>
}}}
### Resources
{{{
<PAGE1>
<DIRECTORY1>
{{{
<PAGE2>
}}}
}}}
}}}
## <SERVICE2>
## <SERVICE3>
}}}
# Exploitation
{{{
## <SERVICE1> -- <TECHNIQUE>
{{{
<TECHNIQUE>
}}}
}}}
# Foothold
{{{
## <USER>
}}}
# Privilege Escalation
{{{
<PRIVESC>
}}}
}}}
